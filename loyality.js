  function mailer(mail) {
    var regexMail=/^[a-z_]+@([a-z]+\.)+[a-z]{2,3}$/;
    var ema=mail.value;
    if (regexMail.test(ema)) {
      document.getElementById('Email').style.color = 'green';
      document.getElementById('Email').innerHTML = "ispravna";
    } else {
      document.getElementById('Email').style.color = 'red';
      document.getElementById('Email').innerHTML = "format nesto@nesto.com";
    }
  }

  function brojevi(postabr) {
    var regexBrojevi=/^\d{5}$/;
    var br=postabr.value;
    if (regexBrojevi.test(br)) {
      document.getElementById('postanskiBroj').style.color = 'green';
      document.getElementById('postanskiBroj').innerHTML = "ispravna";
    } else {
      document.getElementById('postanskiBroj').style.color = 'red';
      document.getElementById('postanskiBroj').innerHTML = "samo 5 brojeva";
    }
  }

  function slovaG(gradovi) {
    var regexSlova=/^[A-Za-z]{3,}$/;
    var gr=gradovi.value;
    if (regexSlova.test(gr)) {
      document.getElementById('grad').style.color = 'green';
      document.getElementById('grad').innerHTML = "ispravna";
    } else {
      document.getElementById('grad').style.color = 'red';
      document.getElementById('grad').innerHTML = "samo slova";
    }
  }
  
    function address(addr) {
    var regexSlBr=/^[A-Za-z0-9\ ]{3,}$/;
    var ad=addr.value;
    if (regexSlBr.test(ad)) {
      document.getElementById('addresa').style.color = 'green';
      document.getElementById('addresa').innerHTML = "ispravna";
    } else {
      document.getElementById('addresa').style.color = 'red';
      document.getElementById('addresa').innerHTML = "samo slova i brojevi razmak izmedu";
    }
  }

  function telef(tele) {
    var regexTel=/^\d{9,}$/;
    var te=tele.value;
    if (regexTel.test(te)) {
      document.getElementById('telefoni').style.color = 'green';
      document.getElementById('telefoni').innerHTML = "ispravna";
    } else {
      document.getElementById('telefoni').style.color = 'red';
      document.getElementById('telefoni').innerHTML = "samo 9 ili vise brojeva";
    }
  }
  
	function ispis()
	{
		//alert("jeliradi");
		
		var kime=document.getElementById('korisnickIme').value;
		var tekst=kime;
		tekst+=document.getElementById('prezime').value;
		tekst+=document.getElementById('ime').value;
		tekst+=document.getElementById('email').value;
		document.getElementById('ispisi').innerHTML = tekst;
		
		
	}